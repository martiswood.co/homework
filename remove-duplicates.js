
function removeDuplicates(arr) {
  return arr.filter((item, index) => arr.indexOf(item) === index);
}

console.log(removeDuplicates([1, 2, 3, 2, 3, 1, 1]));
console.log(removeDuplicates(['a', 1, '2', 'b', 1, '2', 'b']));




function removeDuplicatesSet (arr){
  return [...new Set(arr)];
}

console.log(removeDuplicatesSet([1, 2, 3, 2, 3, 1, 1]));
console.log(removeDuplicatesSet(['a', 1, '2', 'b', 1, '2', 'b']));


