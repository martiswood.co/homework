function createCounter() {
    let counter = -1;

    function IncreaseCounter() {
        return counter += 1;
    };

    return IncreaseCounter;
}

const getCount = createCounter();
console.log (getCount()); // 0:
console.log (getCount()); // 1:
console.log (getCount()); // 2: