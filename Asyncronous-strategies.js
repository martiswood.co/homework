function callback1(data) {
  let sum = 0;
  for (let i = 0; i < data.length; i++) {
    sum = sum + data[i];
  }
  return sum;
}

function callback2(data) {
  let sum = 1;
  for (let i = 0; i < data.length; i++) {
    sum = sum * data[i];
  }
  return sum;
}

function w(s,callback){
    const words=s.split(" ");
    const newArr=[];
    for (let i=0;i<words.length;i++){
        newArr.push(words[i].length);
    }
    return callback(newArr);
}

console.log(w('a bb ccc dddd', callback1)); // result: 10
console.log(w('a bb ccc dddd', callback2)); // result: 24