function isValidJSON(json){
    try {
        JSON.parse(json)
    } catch (error) {
        return false
    }
    return true
}




console.log(isValidJSON('{"a": 2}')); // result: true;
console.log(isValidJSON('{"a: 2')); // result: false;