function multiply(num) {
  let result = num;
  return function a (num2) {
    result = result * num2;
    return a;
  };
}

multiply(2)(4)(6); // 48
multiply(3)(3)(3); // 27
