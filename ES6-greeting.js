function greeting(user) {
  return `Hello, my name is ${user.name} ${user.surname} and I am ${user.age} years old.`;
}

greeting({ name: "Bill", surname: "Jacobson", age: 33 }); // result: Hello, my name is Bill Jacobson and I am 33 years old.
greeting({ name: "Jim", surname: "Power", age: 11 }); // result: Hello, my name is Jim Power and I am 11 years old.
