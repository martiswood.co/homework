function shuffle() {
  for (var i = this.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = this[i];
    this[i] = this[j];
    this[j] = temp;
  }
  return this;
}
Array.prototype.shuffle =shuffle
 
    
[1, 2, 3, 4].shuffle(); // result: some random shuffling ex: [2,3,4,1]
["a", "b", "c"].shuffle(); // result: some random shuffling ex: ['c', 'b', 'a']
