function unique (arr){
    return [...new Set(arr)];
  }


unique([1, 1, 2, 3, 5, 4, 5]); // result: [1, 2, 3, 5, 4]