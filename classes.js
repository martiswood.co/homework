class Guest {
  constructor(tasks) {
    this.tasks = tasks;
  }
  createTask(task){
      console.log(task.name, "is not defind");
  }
  getTask(index) {
    return this.tasks[index];
  }
}
class User extends Guest {
  createTask(task) {
    this.tasks.push(task);
  }
}
class Admin {
  constructor(users) {
      this.users = users;
  }
  changeType(){}
  getArray(){
      return this.users
  }
}

class Task {
  constructor(name) {
    this.name = name;
  }
}

const guest = new Guest([
  new Task("task name 1"),
  new Task("task name 2"),
  new Task("task name 3"),
]);

guest.getTask(0); // { name: 'task name 1' }
guest.getTask(2); // { name: 'task name 3' }
guest.createTask(new Task("task name 4")); // taskName is not defined, should not work

const user = new User([
  new Task("task name 1"),
  new Task("task name 2"),
  new Task("task name 3"),
]);

user.getTask(0); // { name: 'task name 1' }
user.getTask(2); // { name: 'task name 3' }
user.createTask(new Task("task name 4"));
user.getTask(3); // {name: 'task name 4'}

const admin = new Admin([
  new Guest([]),
  new Guest([new Task("task name 1")]),
  new User([]),
  new User([new Task("task name 2")]),
]);

admin.getArray(); // [Guest, Guest, User, User]
admin.changeType(1);
admin.getArray(); // [Guest, User, User, User]
