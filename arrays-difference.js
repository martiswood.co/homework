function arrayDiff(arr1, arr2) {
  if (arr1.length > arr2.length) {
    return arr1.filter((x) => arr2.indexOf(x) === -1);
  } else {
    return arr2.filter((x) => arr1.indexOf(x) === -1);
  }
}

console.log(arrayDiff([1, 2, 3], [1, 2, 3, 4, 5])); // result: [4, 5]
console.log(arrayDiff(["a", "b", "c"], ["a", "b"])); // result: ['c']
