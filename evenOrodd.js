const goThroughNumbers = (start, end) => {
  const arrResult = [];

  for (let i = start; i <= end; i++) {
    if (i % 2 === 0) {
      arrResult.push(`${i} - is Even`);
    } else {
      arrResult.push(`${i} - is Odd`);
    }
  }
  return arrResult
};

console.log(goThroughNumbers(0, 3));
console.log(goThroughNumbers(2, 4));

