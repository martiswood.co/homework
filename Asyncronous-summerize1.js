function summarize1(promise1,promise2){
return new Promise(function(resolve){
    promise1.then(function(arg4){
        promise2.then(function(arg2){
            resolve(arg4+arg2)
        })
    })
});
}


const promise1 = Promise.resolve(4);
const promise2 = new Promise((resolve) => resolve(2));
summarize1(promise1, promise2).then((sum) => {console.log(sum);}); // result: 6