let strenghts = {
    undefined: 0,
    boolean: 1,
    number: 2,
    string: 3,
    object: 5,
    function: 7,
    bigint: 9,
    symbol: 10,
}

function countObjectStrength(obj){
    let ownNames= Object.getOwnPropertyNames(obj)
    let sum = 0
     ownNames.forEach((item)=>{
         sum=sum+strenghts[typeof obj[item]]
     })
     return sum
}



console.log(countObjectStrength(Array)) // 31 (2 + 3 + 5 + 7 + 7 + 7)
console.log(countObjectStrength(Array.prototype)) // 226 (2 + 7 * 32)
console.log(countObjectStrength([]) )// 2
console.log(countObjectStrength({some: 'value'})) // 3