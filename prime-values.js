function primeValues(arr) {
  return arr.map((item) => {
    if (isPrime(item)) {
      return true;
    } else {
      return false;
    }
  });
}

function isPrime(num) {
  if (num <= 3) {
    return true;
  }
  for (let i = 2; i <= num / 2; i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return true;
}

console.log(primeValues([4, 2, 7, 10, 13])); // result: [false, true, true, false, true]
console.log(primeValues([17, 3, 21])); // result: [true, true, false]
