
const result = findUniqueElements ([10, 5, 6, 10, 6, 7]);
function findUniqueElements(list) {
    const uniques = [];
    while(list.length > 0) {
        let current = list.pop()
        let newlist = list.filter(item => item !== current)
        if (list.length === newlist.length) {
            uniques.push(current)
        }
        list = newlist
    }
    return uniques
}

console.log(result)