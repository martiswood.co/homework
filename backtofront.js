console.log(backToFront('hello', 1) );// ohelloo
console.log(backToFront('abc', 3)); // abcabcabc
console.log(backToFront('world', 2));// ldworldld
console.log(backToFront('world', 20));// world

function backToFront(text,count) {
    if (count>text.length) {
        return text
    }
    var lastAdd = text.substr(count*-1)
    text = lastAdd+text+lastAdd
    return text
}