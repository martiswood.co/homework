function numberOfDuplicates(arr) {
  const count = {};
  return arr.map((item) => {
    if (count[item]) {
      count[item] += 1;
    }
    else {
        count[item] = 1;
    }
    return count[item]
  });
}

console.log(numberOfDuplicates([1, 2, 1, 1, 3])); // [1, 1, 2, 3, 1]
console.log(numberOfDuplicates(["a", "a", "aa", "a", "aa"])); // [1, 2, 1, 3, 2]
