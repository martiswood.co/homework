function findNearest(z, x, y) {
  let zx = Math.abs(z - x);
  let zy = Math.abs(z - y);
  if (zx<zy){
      return x;
  }
  return y;
}

console.log(findNearest(100, 22, 122));
console.log(findNearest(50, 22, 122))

