class Logger {
  constructor() {
    this.logs=[];
  }

  log(log) {
      this.logs.push(log)
  }
  getLog() {
      return this.logs
  }
  clearLog() {
      this.logs = []
  }
}

const logger = new Logger();
logger.log("Event 1");
logger.log("Event 2");
logger.getLog(); // ['Event 1', 'Event 2']
logger.clearLog();
logger.getLog(); // []

console.log(logger)