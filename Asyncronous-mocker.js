function mocker(data) {
  return function(){
    return new Promise(function(resolve) {
        setTimeout(function() {
          resolve(data);
        }, 1000);
      });
  }
  
}

const getUsers = mocker([{ id: 1, name: "User1" }]);
console.log(getUsers)
getUsers().then((users) => {
  // Will fire after 1 second.
  console.log(users); // result: [{id: 1, name: 'User1'}];
});
