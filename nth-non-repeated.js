function nThNoRepeatedValue(arr, num) {
  const returnUniques = removeDuplicates(arr);
  return returnUniques[num - 1];
}

function removeDuplicates(arr) {
  return arr.filter((item) => {
    let newArr = arr.filter((item2) => item === item2);
    return newArr.length === 1;
  });
}
console.log(nThNoRepeatedValue([321, 43, 3213, 1689], 2)); // result: 43 (among 4 unique elements the 2nd is 43)
console.log(nThNoRepeatedValue([1, 1, 3, 4, 3, 10], 1)); // result: 4 (among 2 unique elements the 1st is 4)
console.log(nThNoRepeatedValue([1, 2, 1, 1], 1)); // result: 2 (among 1 unique elements the 1st is 2)
