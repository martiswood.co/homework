async function summarize2(promise1,promise2){
const arg4=await promise1;
const arg2=await promise2;
return arg4+arg2;
}


const promise1 = Promise.resolve(4);
const promise2 = new Promise((resolve) => resolve(2));
summarize2(promise1, promise2).then((sum) => {console.log(sum);}); // result: 6