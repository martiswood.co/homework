function* generator(arr){
for (let i=0;i<arr.length;i++){
    yield arr[i]
}
}



const it = generator(['brick', 'plate', 'minifigure', 'slope']);
console.log(it.next().value); // return 'brick'
console.log(it.next().value); // return 'plate'