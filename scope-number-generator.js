function createCounter(num = 0) {
  let result = num;
  return function () {
    result = result + 1;
    console.log(result);
  };
}

const counter = createCounter(44);
counter(); // 45
counter(); // 46
counter(); // 47
